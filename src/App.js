import React, { Component } from "react"
import "./App.css"
import data from "./Data/data.json"

/* components import */
import Search from "./Components/Search"
import Menubar from "./Components/Menubar"
import Results from "./Components/Results"
import SpotiCallback from "./Components/SpotiCallback"

import { Router, Route, browserHistory } from "react-router"
import { observer, inject } from "mobx-react"

class App extends Component {
  componentDidMount() {
    this.props.Main.getPlaylist()
    this.getPlaylist = setInterval(this.props.Main.getPlaylist, 30000)
  }
  componentWillUnmount() {
    clearInterval(this.getPlaylist)
  }
  render() {
    return (
      <section>
        <Menubar />
        <div className="container">
          <div className="col-sm-3">
            <Search />
          </div>
          <div className="col-sm-9">
            <Router history={browserHistory}>
              <Route path="/" component={Results} />
              <Route path="/callback" component={SpotiCallback} />
              <Route path="/menu" component={Menubar} />
            </Router>
          </div>
        </div>
      </section>
    )
  }
}

export default inject("Main")(observer(App))
