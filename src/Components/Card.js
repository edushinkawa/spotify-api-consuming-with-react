import React, { Component } from "react"

const Card = props => (
  <section className="Card col-sm-4">
    <img src={props.images[0].url} className="img-responsive" width={200}/>
    <p>{props.name}</p>
  </section>
)

export default Card
