import React, { Component } from "react"
import DayPickerInput from "react-day-picker/DayPickerInput"
import "react-day-picker/lib/style.css"
import logo from "../Images/logo.png"
import { observer, inject } from "mobx-react"
import Loader from "./Loader"
import moment from 'moment'

const Search = props => (
  <section className="Search">
    <img src={logo} height={80} />
    <select
      className="custom-select"
      value={props.Main.valueLocale}
      onChange={props.Main.handleChangeLocale}
    >
      {props.Main.menuItemsData.filters[0].values.map((item, index) => (
        <option value={item.value}>{item.name}</option>
      ))}
    </select>
    <select
      className="custom-select"
      value={props.Main.country}
      onChange={props.Main.handleChangeCountry}
    >
      <option value={null}>Escolha o país</option>
      {props.Main.menuItemsData.filters[1].values.map((item, index) => (
        <option value={item.value}>{item.name}</option>
      ))}
    </select>
    <DayPickerInput
      name="birthday"
      placeholder="DD/MM/YYYY"
      format="DD/MM/YYYY"
      value={props.Main.valueDate}
      onDayChange={props.Main.handleDayChangeDate}
      autocomplete={false}
    />
    <select
      className="custom-select"
      value={props.Main.valueLimit}
      onChange={props.Main.handleChangeLimit}
    >
      <option value={null}>Limite de resultados</option>
      {props.Main.menuItemsData.filters[3].values.map((item, index) => (
        <option value={item.value}>{item.name}</option>
      ))}
    </select>
    {props.Main.loading && <Loader />}
  </section>
)

export default inject("Main")(observer(Search))
