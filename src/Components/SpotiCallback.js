import React, { Component } from "react"
import { Router, Route, browserHistory } from "react-router"
import { observer, inject } from "mobx-react"

class Spoticallback extends Component {
  componentWillMount() {
    console.log("Loading API")
    sessionStorage.setItem(
      "access_token",
      this.props.location.query.access_token
    )
    this.props.Main.getPlaylist()
    browserHistory.push("/")
  }

  render() {
    return <section>Credentials validated!</section>
  }
}

export default inject("Main")(observer(Spoticallback))
