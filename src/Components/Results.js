import React, { Component } from "react"
import { observer, inject } from "mobx-react"

/* components import */
import Card from "./Card"

const Results = props => (
  <section className="Results">
    <h1>{props.Main.message}</h1>
    {props.Main.spotify.map((item, index) => <Card {...item} />)}
  </section>
)

export default inject("Main")(observer(Results))
