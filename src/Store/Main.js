import { extendObservable, computed, action, autorun } from "mobx"
import axios from "axios"
import data from "../Data/data.json"
import SpotiClientId from "../Config/SpotiConfig"
import moment from "moment"

class Main {
  constructor() {
    extendObservable(this, {
      menuItemsData: data,
      spotify: [],
      locale: null,
      country: null,
      limit: null,
      selectedDay: null,
      loading: false,
      message: "",
      valueLocale: null
    })
  }

  handleChangeLocale = event => {
    this.locale = event.target.value
    this.getPlaylist()
  }

  handleChangeCountry = event => {
    this.country = event.target.value
    this.getPlaylist()
  }

  handleChangeLimit = event => {
    this.limit = event.target.value
    this.getPlaylist()
  }

  handleDayChangeDate = selectedDay => {
    this.selectedDay = selectedDay.format("YYYY-MM-DDTHH:mm:ss")
    this.getPlaylist()
  }

  getPlaylist = () => {
    const config = {
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem("access_token")}`
      }
    }
    config.params = {
      locale: this.locale,
      country: this.country,
      limit: this.limit,
      timestamp: this.selectedDay
    }
    this.loading = true
    return axios
      .get("https://api.spotify.com/v1/browse/featured-playlists", config)
      .then(res => {
        this.spotify = res.data.playlists.items
        this.message = res.data.message
        this.loading = false
      })
      .catch(error => (this.loading = false))
  }
}

export default new Main()
